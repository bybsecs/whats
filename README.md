# Whats - WhatsApp Desktop

Unofficial webapp for WhatsApp Web using python and PyQt5 with PyQtWebEngine.

### Required packages:

- python >= 3.6
- PyQt5 >= 5.15.5
- PyQtWebEngine >= 5.15.5

### Installation instructions:

To install "Whats", use the following commands:
```sh
$ git clone https://github.com/mxnt10/whats.git
$ cd whats

$ sudo ./install.sh
```

### GNU General Public License

This repository has scripts that were created to be free software.<br/>
Therefore, they can be distributed and / or modified within the terms of the *GNU General Public License*.

>[General Public License](https://pt.wikipedia.org/wiki/GNU_General_Public_License)
>
>Free Software Foundation (FSF) Inc. 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

### Comments

In case of bugs, problems of execution or construction of packages, constructive criticism, among others, please submit a message to one of the contacts below.

### Contact

Maintainer: Mauricio Ferrari<br/>
E-Mail: *m10ferrari1200@gmail.com*<br/>
Telegram: *@maurixnovatrento*<br/>
